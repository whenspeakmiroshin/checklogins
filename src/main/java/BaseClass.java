import com.jayway.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class BaseClass {
    protected String getEnv(String env){
        return System.getenv(env);
    }

    protected List<String[]> readFromFile(String url) {
        List<String[]> lines =  new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line.split(getEnv("delimiter")));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    @DataProvider
    private Object[][] getData(){
        List<String[]> list = readFromFile(getEnv("filePath"));
        Object[][] objects = new  Object[list.size()][2];
        for (int i = 0; i < list.size();i++){
            System.arraycopy(list.get(i), 0, objects[i], 0, list.get(i).length);
        }
        return objects;
    }

    @Test(dataProvider = "getData")
    public void smtTest(String login, String passwrd){
        RestAssured.baseURI = getEnv("url");
        RestAssured.given()
                .queryParam("email",login)
                .queryParam("password",passwrd)
                .when().log().all()
                .post()
                .then()
                    .log().all()
                .body("sessionToken", Matchers.notNullValue());
    }
}
